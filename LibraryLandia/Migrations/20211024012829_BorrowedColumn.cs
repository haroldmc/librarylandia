﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LibraryLandia.Migrations
{
    public partial class BorrowedColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "BorrowedBooks",
                table: "Books",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BorrowedBooks",
                table: "Books");
        }
    }
}
