using System.ComponentModel;

namespace LibraryLandia.Models
{
    public class Category
    {
        public int Id { get; set; }
        [DisplayName("Nombre")]
        public string Name { get; set; }
    }
}