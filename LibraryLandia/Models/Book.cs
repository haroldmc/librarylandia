using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibraryLandia.Models
{
    public class Book
    {
        public int Id { get; set; }
        [DisplayName("Título")]
        public string Title { get; set; }
        [DisplayName("Autor")]
        public int AuthorId { get; set; }
        [DisplayName("Categoría")]
        public int CategoryId { get; set; }
        [DisplayName("Edición")]
        public string Edition { get; set; }
        [DisplayName("Ejemplares")]
        public long CopiesQuantity { get; set; }
        [DisplayName("Prestados")]
        public long BorrowedBooks { get; set; }

        [DisplayName("Disponibles")]
        [NotMapped]
        public long AvailableBooks => CopiesQuantity - BorrowedBooks;

        #region Collections
        [DisplayName("Autor")]
        public Author Author { get; set; }
        [DisplayName("Categoría")]
        public Category Category { get; set; }
        #endregion
    }
}