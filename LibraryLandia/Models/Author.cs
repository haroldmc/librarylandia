using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibraryLandia.Models
{
    public class Author
    {
        public int Id { get; set; }
        [DisplayName("Nombre")]
        public string Name { get; set; }
    }
}