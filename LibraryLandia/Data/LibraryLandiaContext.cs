using LibraryLandia.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryLandia.Data
{
    public class LibraryLandiaContext : DbContext
    {
        public LibraryLandiaContext(DbContextOptions<LibraryLandiaContext> options) : base(options) {}

        #region Tables
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Category> Categories { get; set; }
        #endregion
        
    }
}